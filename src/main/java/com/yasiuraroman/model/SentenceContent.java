package com.yasiuraroman.model;

public interface SentenceContent {
    String getValue();
    void setValue(String value);
    ContentType getType();
}
