package com.yasiuraroman.model;

public class PunctuationMark implements SentenceContent {
    private String value;
    private ContentType type;

    public PunctuationMark(String value, ContentType type) {
        this.value = value;
        this.type = type;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public ContentType getType() {
        return type;
    }

    public void setType(ContentType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return getValue();
    }
}
