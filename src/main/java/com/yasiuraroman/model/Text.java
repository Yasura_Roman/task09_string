package com.yasiuraroman.model;

import java.util.List;
import java.util.stream.Collectors;

public class Text {
    private List<Sentence> content;

    public List<Sentence> getContent() {
        return content;
    }

    public void setContent(List<Sentence> content) {
        this.content = content;
    }

    public Text(List<Sentence> content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return content.stream().map( e -> e.toString()).collect(Collectors.joining(" "));
    }
}
