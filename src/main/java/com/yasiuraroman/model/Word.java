package com.yasiuraroman.model;

public class Word implements SentenceContent {
    private String value;

    public Word(String value) {
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public ContentType getType() {
        return ContentType.WORD;
    }

    @Override
    public String toString() {
        return getValue();
    }
}
