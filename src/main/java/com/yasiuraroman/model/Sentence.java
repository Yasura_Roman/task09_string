package com.yasiuraroman.model;

import java.util.List;
import java.util.stream.Collectors;

public class Sentence {
    private List<SentenceContent> content;

    public Sentence(List<SentenceContent> content) {
        this.content = content;
    }

    public List<SentenceContent> getContent() {
        return content;
    }

    public void setContent(List<SentenceContent> content) {
        this.content = content;
    }

    public void addContent(SentenceContent content){
        this.content.add(content);
    }

    public boolean isValidSentence(){
        if ( content.get(content.size() - 1).getType().equals(ContentType.PUNCTUATION_MARK_END)
                && content.stream()
                .map( e -> e.getType())
                .filter(e -> e.equals(ContentType.PUNCTUATION_MARK_END)).count() == 1
        ){
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return content.stream().map(e -> e.toString()).collect(Collectors.joining(" "));
    }
}
