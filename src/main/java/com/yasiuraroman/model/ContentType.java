package com.yasiuraroman.model;

public enum ContentType{
    WORD,
    PUNCTUATION_MARK_INTERNAL,
    PUNCTUATION_MARK_END
}
