package com.yasiuraroman.controller;

import com.yasiuraroman.model.Word;

import java.util.List;

public interface TaskController {
    void task1();
    void task2();
    void task3();
    void task4(int length);
    void task5();
    void task6();
    void task7();
    void task8();
    void task9(String letter);
    void task10(List<Word> list);
    void task11(String start,String end);
    /*
    void task12();
    void task13();
    void task14();
    void task15();
    void task16();
     */
}
