package com.yasiuraroman.controller;

import com.yasiuraroman.model.Text;
import com.yasiuraroman.model.Word;
import com.yasiuraroman.tasks.BigTask;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class TaskControllerImp implements TaskController {

    BigTask model;

    public TaskControllerImp(Text text, Logger logger) {
        model = new BigTask(text, logger);
    }

    @Override
    public void task1() {
        model.task1();
    }

    @Override
    public void task2() {
        model.task2();
    }

    @Override
    public void task3() {
        model.task3();
    }

    @Override
    public void task4(int length) {
        model.task4(length);
    }

    @Override
    public void task5() {
        model.task5();
    }

    @Override
    public void task6() {
        model.task6();
    }

    @Override
    public void task7() {
        model.task7();
    }

    @Override
    public void task8() {
        model.task8();
    }

    @Override
    public void task9(String letter) {
        model.task9(letter);
    }

    @Override
    public void task10(List<Word> list) {
        model.task10(list);
    }

    @Override
    public void task11(String start, String end) {
        model.task11(start,end);
    }
}
