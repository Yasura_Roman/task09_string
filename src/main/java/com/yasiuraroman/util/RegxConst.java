package com.yasiuraroman.util;

public class RegxConst {
    public static final String SENTENCE_DELIMITER = "[^.!?]+([.!?])";
    public static final String SENTENCE_CONTENT_DELIMITER = "\\S+";
    public static final String SENTENCE_CONTENT_PATTERN = "\\w+|[,-_]";
    public static final String SENTENCE_CONTENT_WORD = "\\w+";
    public static final String SENTENCE_CONTENT_PUNCTUATION = "[,-_]";
    public static final String SENTENCE_CONTENT_END = "[.!?]";

}
