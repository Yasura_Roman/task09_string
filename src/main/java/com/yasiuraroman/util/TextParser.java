package com.yasiuraroman.util;

import com.yasiuraroman.model.*;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextParser {

    public List<Sentence> parseText(String text){
        List<Sentence> list = new LinkedList<>();
        Pattern pattern = Pattern.compile(RegxConst.SENTENCE_DELIMITER);
        Matcher matcher = pattern.matcher(text);
        while (matcher.find()){
            matcher.group(0);
            matcher.group(1);
            list.add(new Sentence(parseSentence(matcher.group(0))));
        }
        return list;
    }

    private List<SentenceContent> parseSentence(String sentence){
        List<SentenceContent> list = new LinkedList<>();
        Pattern pattern = Pattern.compile(RegxConst.SENTENCE_CONTENT_DELIMITER);
        Matcher matcher = pattern.matcher(sentence);
        while (matcher.find()){
            list.addAll(parseSentenceParts(matcher.group(0)));
        }
        return list;
    }

    private List<SentenceContent> parseSentenceParts(String sentenceParts){
        List<SentenceContent> list = new LinkedList<>();
        Pattern pattern = Pattern.compile(RegxConst.SENTENCE_CONTENT_PATTERN);
        Matcher matcher = pattern.matcher(sentenceParts);
        while (matcher.find()){
            String mat = matcher.group(0);
            if (mat.matches(RegxConst.SENTENCE_CONTENT_WORD)){
                list.add(new Word(mat));
            } else if (mat.matches(RegxConst.SENTENCE_CONTENT_PUNCTUATION)){
                list.add(new PunctuationMark(mat, ContentType.PUNCTUATION_MARK_INTERNAL));
            } else if (mat.matches(RegxConst.SENTENCE_CONTENT_END)){
                list.add(new PunctuationMark(mat, ContentType.PUNCTUATION_MARK_END));
            }
        }
        return list;
    }
}
