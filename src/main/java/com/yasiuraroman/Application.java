package com.yasiuraroman;

import com.yasiuraroman.view.GeneralView;

public class Application {
    public static void main(String[] args) {
        GeneralView view = new GeneralView();
        view.show();
    }
}
