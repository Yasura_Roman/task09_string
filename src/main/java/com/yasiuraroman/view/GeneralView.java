package com.yasiuraroman.view;

import com.yasiuraroman.controller.TaskController;
import com.yasiuraroman.controller.TaskControllerImp;
import com.yasiuraroman.model.Text;
import com.yasiuraroman.model.Word;
import com.yasiuraroman.util.TextParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class GeneralView {
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger();
    private ResourceBundle menuBundle = ResourceBundle.getBundle("Menu");
    private Text text;

    public GeneralView() {

        TaskController taskController = new TaskControllerImp(text,logger);
        TextParser textParser = new TextParser();
        menu = new LinkedHashMap<>();
        createMenu();
        methodsMenu = new LinkedHashMap<>();

        methodsMenu.put("P", () -> {
            logger.info("Введіть шлях до файлу");
            String filePath = input.nextLine();

            try {
                String fileContent = Files.lines(Path.of(filePath)).reduce((s1,s2) -> s1.concat(s2)).get();
                logger.info(fileContent);
                text = new Text(textParser.parseText(fileContent));
            } catch (IOException e) {
                logger.error("File not found");
            }

        });
        methodsMenu.put("L1", () -> setLanguage("en"));
        methodsMenu.put("L2", () -> setLanguage("uk"));
        methodsMenu.put("L3", () -> setLanguage("sp"));
        methodsMenu.put("T1", taskController::task1);
        methodsMenu.put("T2", taskController::task2);
        methodsMenu.put("T3", taskController::task3);
        methodsMenu.put("T4", () -> {
            taskController.task4(3);
        });
        methodsMenu.put("T5", taskController::task5);
        methodsMenu.put("T6", taskController::task6);
        methodsMenu.put("T7", taskController::task7);
        methodsMenu.put("T8", taskController::task8);
        methodsMenu.put("T9", () -> {
            taskController.task9("a");
        });
        methodsMenu.put("T10", () -> {
            taskController.task10(Arrays.asList(new Word[]{new Word("Hello"),new Word("Word"),new Word("Tom")}));
        });
        methodsMenu.put("T11", () -> {
            taskController.task11("H","o");
        });
    }

    private void setLanguage(String l18n) {
        menuBundle = ResourceBundle.getBundle("Menu",new Locale(l18n));
        createMenu();
    }

    private void createMenu(){
        SortedSet<String> sortedSet = new TreeSet<>();
        sortedSet.addAll(menuBundle.keySet());
        for (String key : sortedSet
        ) {
            menu.put(key,menuBundle.getString(key));
        }
        menu.put("Q", "  Q - exit");
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        logger.info("MENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
