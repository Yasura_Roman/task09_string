package com.yasiuraroman.tasks;

import com.yasiuraroman.model.*;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class BigTask {

    private Text text;
    private Logger logger;

    public BigTask(Text text, Logger logger) {
        this.text = text;
        this.logger = logger;
    }

    public void task1(){
        Map.Entry<String,Long> max = text.getContent().stream()
                .flatMap( s -> s.getContent().stream()
                        .filter(e -> e.getType().equals(ContentType.WORD))
                        .map( w -> w.getValue())
                        .distinct()
                )
                .collect(Collectors.groupingBy(e -> e, Collectors.counting()))
                .entrySet()
                .stream()
                .max( (e1, e2) -> (int) (e1.getValue()-e2.getValue())).get();
        logger.info(max);
    }

    private List<Word> wordInText(){
        return (List<Word>) text.getContent().stream()
                .flatMap(sentence -> sentence.getContent().stream())
                .filter(e -> e.getType().equals(ContentType.WORD))
                .map( e -> (Word) e)
                .collect(Collectors.toList());
    }

    private List<Word> uniqueWrdInText(){
        return wordInText().stream()
                .map( e -> {
                    e.setValue(e.getValue().toLowerCase());
                    return e;
                }
                )
                .distinct()
                .collect(Collectors.toList());
    }

    public void task2(){
        Map<Sentence,Long> wordCount = text.getContent().stream()
                .collect(Collectors.toMap(s -> s, s -> s.getContent()
                                .stream()
                        .filter(e -> e.getType()
                                .equals(ContentType.WORD))
                                .count()
                        )
                );

        List<Sentence> result =  wordCount.entrySet().stream()
                .sorted((e1,e2) -> e1.getValue().compareTo(e2.getValue()))
                .map(e -> e.getKey())
                .collect(Collectors.toList());

        for (Sentence sen:result
             ) {
            logger.info(sen.toString());
        }
    }

    public void task3(){
        List<Word> wordInFirstSentence = text.getContent().stream()
                .limit(1)
                .flatMap(sentence -> sentence.getContent().stream())
                .filter(e -> e.getType().equals(ContentType.WORD))
                .map( e -> (Word) e)
                .collect(Collectors.toList());

        for (Word word:wordInFirstSentence
             ) {
            if( wordInText().stream()
                    .filter(w -> w.getValue().equals(word.getValue())).count() == 1 ){
                logger.info(word.getValue());
                return;
            }
        }
        logger.info("First sentence have not unique word");
    }

    public void task4(int length){
        List<Word> result = text.getContent().stream()
                .filter( e -> e.getContent().get(e.getContent().size() - 1).getValue().equals("?"))
                .flatMap(e -> e.getContent().stream())
                .filter(e -> e.getType().equals(ContentType.WORD))
                .map(e -> (Word) e)
                .filter(e -> e.getValue().length() == length)
                .distinct()
                .collect(Collectors.toList());

        if (result.size() == 0){
            logger.info("word not exist");
            return;
        }
        for (Word w:result
        ) {
            logger.info(w.getValue());
        }
    }

    private Word wordWithMaxLength(){
        return uniqueWrdInText().stream()
                .max(Comparator.comparingInt(e -> e.getValue().length()))
                .get();
    }

    public void task5(){
        //TODO переробити
        text.getContent().stream()
                .forEach( s ->{
                            Word maxLength = s.getContent().stream()
                                    .filter( w -> w.getType().equals(ContentType.WORD))
                                    .map( w -> (Word) w)
                                    .max( Comparator.comparingInt(w -> w.getValue().length()))
                                    .get();

                            Word firsWordWhichStartVowel = s.getContent().stream()
                                    .filter( w -> w.getType().equals(ContentType.WORD))
                                    .map( w -> (Word) w)
                                    .filter(w -> w.getValue().matches("^[aeyuioAEYUIO].*"))
                                    .findFirst()
                                    .orElse(new Word(""));

                            if (firsWordWhichStartVowel.getValue() != ""){
                                int index1 = s.toString().indexOf(maxLength.getValue());
                                int index2 = s.toString().indexOf(firsWordWhichStartVowel.getValue());
                                String[] text = new String[5];
                                if (index1 > index2){
                                    text[0] = s.toString().substring(0,index2);
                                    text[1] = s.toString().substring(index2,firsWordWhichStartVowel.getValue().length()+index2);
                                    text[2] = s.toString().substring(firsWordWhichStartVowel.getValue().length()+index2,index1);
                                    text[3] = s.toString().substring(index1,maxLength.getValue().length()+index1);
                                    text[4] = s.toString().substring(maxLength.getValue().length()+index1);
                                    logger.info(text[0]+text[3]+text[2]+text[1]+text[4]);
                                } else if (index2 > index1){
                                    text[0] = s.toString().substring(0,index1);
                                    text[1] = s.toString().substring(index1,maxLength.getValue().length()+index1);
                                    text[2] = s.toString().substring(maxLength.getValue().length()+index1,index2);
                                    text[3] = s.toString().substring(index2,firsWordWhichStartVowel.getValue().length()+index2);
                                    text[4] = s.toString().substring(firsWordWhichStartVowel.getValue().length()+index2);
                                    logger.info(text[0]+text[3]+text[2]+text[1]+text[4]);
                                } else {
                                    logger.info(s.toString());
                                }
                            } else {
                                logger.info(s.toString());
                            }
                        }
                );
    }

    public void task6(){
        Map<Character, List<Word>>  resultMap =  wordInText().stream()
                .collect(Collectors
                        .groupingBy(e -> e.getValue().toLowerCase().charAt(0))
                );

        for (Character c : resultMap.keySet()
        ) {
            for (Word w : resultMap.get(c)
            ) {
                logger.info(w.getValue());
            }
            logger.info("");
        }
    }

    public void task7(){
        List<Word> result = uniqueWrdInText().stream()
                .sorted(Comparator.comparingDouble(e -> {
                    int vowelCount = 0;
                    for (char c : e.getValue().toCharArray()) {
                        if (isVowel(c)){
                            vowelCount++;
                        }
                    }
                    return (vowelCount*100/e.getValue().length());
                }))
                .collect(Collectors.toList());

        for (Word w:result
             ) {
            logger.info(w.getValue());
        }
    }

    private boolean isVowel(char c){
        if (c == 'e' || c == 'y' || c == 'u' || c == 'i' || c == 'o' || c == 'a'
                || c == 'E' || c == 'Y' || c == 'U' || c == 'I' || c == 'O' || c == 'A'){
            return true;
        }
        return false;
    }

    public void task8(){
        List<Word> result = uniqueWrdInText().stream()
                .filter(w -> w.getValue().matches("^[aeyuioAEYUIO].*"))
                .sorted(BigTask::compareByFirstConsonantalReversOrder)
                .collect(Collectors.toList());

        for (Word w:result
        ) {
            logger.info(w.getValue());
        }
    }

    private static int compareByFirstConsonantalReversOrder(Word w1, Word w2){
        return compareByFirstConsonantal(w2,w1);
    }

    private static int compareByFirstConsonantal(Word w1, Word w2) {
        int value1 = Integer.MIN_VALUE;
        int value2 = Integer.MIN_VALUE;
        Pattern pattern = Pattern.compile("[^aeyuio].*");
        Matcher matcher = pattern.matcher(w1.getValue().toLowerCase());
        if (matcher.find()){
            value1 = matcher.start();
        } else {
            return -1;
        }
        matcher = pattern.matcher(w2.getValue().toLowerCase());
        if (matcher.find()){
            value2 = matcher.start();
        } else {
            return 1;
        }
        return w1.getValue().indexOf(value1) -  w2.getValue().indexOf(value2);
    }

    public void task9(String letter){
        List<Word> result = uniqueWrdInText().stream()
                .sorted( (e1, e2) -> {
                    int res =howMathLetterInWord(e1,letter) - howMathLetterInWord(e2,letter);
                    if (res == 0){
                        return e1.getValue().compareTo(e2.getValue());
                    }else {
                    return res;
                    }
                } )
                .collect(Collectors.toList());

        for (Word w:result
        ) {
            logger.info(w.getValue());
        }
    }

    private int howMathLetterInWord(Word word, String letter){
        int count = 0;
        Pattern pattern = Pattern.compile(letter.toLowerCase());
        Matcher matcher = pattern.matcher(word.getValue().toLowerCase());
        while (matcher.find()){
            count++;
        }
        return count;
    }

    public void task10(List<Word> list){
        Map<Word,Map<Sentence,Long>> result = new HashMap<>() ;
        for (Word word:list
        ) {
            Map<Sentence,Long> countMap = text.getContent().stream()
                    .collect(Collectors.toMap(s -> s, s -> s.getContent().stream()
                            .filter(w -> w.getValue().equals(word.getValue()))
                            .count())
                    );
            result.put(word,countMap);
        }
        Map<Word,Long> sortMap = new HashMap<>();
        for (Word word:result.keySet()
        ) {
            Long value = result.get(word).entrySet().stream().mapToLong(e -> e.getValue()).sum();
            sortMap.put(word,value);
        }
        sortMap.entrySet().stream().sorted( (e1,e2) -> (int) (e1.getValue() - e2.getValue())).forEach( mp ->
                {
                        logger.info("Word '" + mp.getKey() + "' :");
        result.get(mp.getKey()).entrySet().stream()
                .forEach( e -> logger.info("Sentence : " + e.getKey() + " - " + e.getValue()));
                }
        );

    }

    public void task11 (String start,String end){
        logger.info("Text:");
        logger.info(text);
        logger.info("delete " + start + "..." + end);
        logger.info("New text:");
        text.getContent().stream()
                .map( s -> s.toString())
                .map(s -> s.replaceAll(start + "[^.?!]*" +end, ""))
                .forEach( s -> logger.info(s));
    }
}
